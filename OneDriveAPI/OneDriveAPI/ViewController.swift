//
//  ViewController.swift
//  OneDriveAPI
//
//  Created by Teiksma  on 01.03.18.
//  Copyright © 2018. g. janisengers. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, UITableViewDelegate, UITableViewDataSource
{
    let clientID    = "a2a87fa9-d996-4126-a05f-e1a3f1f00763"
    let scope       = "files.readwrite"
    let redirectUri = "msala2a87fa9-d996-4126-a05f-e1a3f1f00763://auth"
    
    var menuView: UIView!
    var fileView: UIView!
    var webView: WKWebView!
    var myTableView: UITableView!
    
    var tableViewIntialized: Bool = false
    var parentFolderID: String?
    var accessToken: String?
    var tableViewContent: NSArray?
    var frameSize:[String:CGFloat] = [String:CGFloat]()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.frameSize["statusBarHeight"]   = UIApplication.shared.statusBarFrame.size.height
        self.frameSize["width"]             = self.view.frame.width
        self.frameSize["height"]            = self.view.frame.height - self.frameSize["statusBarHeight"]!
        
        self.loadMenu()
    }
    
    func loadMenu()
    {
        for button in (self.view.subviews.filter{$0 is UIButton})
        {
            button.removeFromSuperview()
        }
        
        self.menuView = UIView(frame: CGRect(x: 0, y:  self.frameSize["statusBarHeight"]!, width: self.frameSize["width"]!, height: self.frameSize["height"]!))
        
        let button1 = UIButton(type: UIButtonType.system) as UIButton
        button1.setTitle("OneDrive", for: UIControlState.normal)
        button1.addTarget(self, action: #selector(ViewController.menuButtonAction(_:)), for: .touchUpInside)
        button1.frame = CGRect(x:0, y:0, width:self.frameSize["width"]!, height:self.frameSize["height"]! / 2)
        button1.tag = 1
        button1.backgroundColor = UIColor.blue
        button1.tintColor = UIColor.white
        button1.titleLabel?.font = UIFont(name: "Helvetica", size: 20)
        
        let button2 = UIButton(type: UIButtonType.system) as UIButton
        button2.setTitle("Local", for: UIControlState.normal)
        button2.addTarget(self, action: #selector(ViewController.menuButtonAction(_:)), for: .touchUpInside)
        button2.frame = CGRect(x:0, y:self.frameSize["height"]! / 2, width:self.frameSize["width"]!, height:self.frameSize["height"]! / 2)
        button2.tag = 2
        button2.backgroundColor = UIColor.white
        button2.tintColor = UIColor.blue
        button2.titleLabel?.font = UIFont(name: "Helvetica", size: 20)
        
        self.menuView.addSubview(button1)
        self.menuView.addSubview(button2)
        
        self.view.addSubview(self.menuView)
    }
    
    func openAuthorizationPage()
    {
        let authUrl = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=" + clientID + "&scope=" + scope + "&response_type=code&redirect_uri=" + redirectUri
        let request = URLRequest(url: URL(string: authUrl)!)
        
        self.webView = WKWebView(frame: view.frame, configuration: WKWebViewConfiguration())
        view.addSubview(webView)
        
        self.webView.navigationDelegate = self as WKNavigationDelegate;
        self.webView.load(request)
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!)
    {
        let data = getDataFromUrl(targetUrl: URL(string:(self.webView.url?.absoluteString)!)!)
        
        if let code = data["code"]
        {
            self.getToken(code: code)
        }
    }
    
    func getToken(code: String)
    {
        let tokenUrl    = URL(string: "https://login.microsoftonline.com/common/oauth2/v2.0/token")!
        let dataString  = "client_id=" + clientID + "&redirect_uri=" + redirectUri + "&code=" + code + "&grant_type=authorization_code"
        
        var request         = URLRequest(url: tokenUrl)
        request.httpBody    = dataString.data(using: .utf8)
        request.httpMethod  = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else
            {
                print("Error: \(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {
                print("Error: status code \(httpStatus.statusCode)")
                print("Response = \(String(describing: response))")
            }
            
            let responseString  = String(data: data, encoding: .utf8)
            let responseData    = self.jsonToDictionary(jsonString: responseString!)
            
            self.accessToken = responseData["access_token"] as? String
            
            DispatchQueue.main.async
            {
                self.getFolderContent()
                self.webView.removeFromSuperview()
            }
        }
        task.resume()
    }
    
    func getFolderContent(folderID:String = "")
    {
        let url = (folderID == "")
            ? URL(string: "https://graph.microsoft.com/v1.0/me/drive/root/children")!
            : URL(string: "https://graph.microsoft.com/v1.0/me/drive/items/" + folderID + "/children")!

        var request = URLRequest(url: url)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(self.accessToken!)",forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else
            {
                print("Error: \(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {
                print("Error: status code \(httpStatus.statusCode)")
                print("Response = \(String(describing: response))")
            }
            
            let responseString  = String(data: data, encoding: .utf8)
            let responseData    = self.jsonToDictionary(jsonString: responseString!)

            DispatchQueue.main.async
            {
                self.setParentFolderID(folderID: folderID)                
                self.tableViewContent = self.prepareTableContentData(data: responseData)

                (self.tableViewIntialized)
                    ? self.myTableView.reloadData()
                    : self.initTableView()
            }
        }
        task.resume()
    }
    
    func setParentFolderID(folderID: String)
    {
        self.parentFolderID = nil
        
        if folderID != ""
        {
            let url = URL(string: "https://graph.microsoft.com/v1.0//me/drive/items/" + folderID)!
            
            var request = URLRequest(url: url)
            
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.setValue("Bearer \(self.accessToken!)",forHTTPHeaderField: "Authorization")
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                
                guard let data = data, error == nil else
                {
                    print("Error: \(String(describing: error))")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
                {
                    print("Error: status code \(httpStatus.statusCode)")
                    print("Response = \(String(describing: response))")
                }
                
                let responseString  = String(data: data, encoding: .utf8)
                let responseData    = self.jsonToDictionary(jsonString: responseString!)
                
                if var _ = responseData["parentReference"]
                {
                    let parentData = responseData["parentReference"] as! [String : String]
                    self.parentFolderID = parentData["id"]
                }
            }
            task.resume()
        }
    }
    
    func prepareTableContentData(data: [String:AnyObject])->NSMutableArray
    {
        let contentData: NSMutableArray = []
        
        if let values = data["value"]
        {
            for value in values as! [Any]
            {
                var item = [String:String]()
                
                let val = value as! Dictionary<String, AnyObject>
                
                item["id"]      = val["id"] as? String
                item["name"]    = val["name"] as? String
                    
                if let folder = val["folder"]
                {
                    let folder = folder as! Dictionary<String, AnyObject>
                    
                    item["type"]        = "folder"
                    item["childCount"]  = "\(folder["childCount"] as! Int)"
                }
                else
                {
                    item["type"] = "file"
                    item["webUrl"]  = val["webUrl"] as? String
                }
                    
                contentData.add(item)
            }
        }
        
        return contentData
    }
    
    func openLocalDirectory()
    {
        let contentData: NSMutableArray = []
        
        let fileManager     = FileManager.default
        let documentsURL =   fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        do
        {
            let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            
            for file in fileURLs
            {
                var item = [String:String]()
                
                item["localUrl"]    = "\(file)"
                item["name"]        = file.lastPathComponent
                
                if (item["name"] != ".DS_Store")
                {
                    contentData.add(item)
                }
            }
            
            self.tableViewContent = contentData
            
            (self.tableViewIntialized)
                ? self.myTableView.reloadData()
                : self.initTableView()
        }
        catch
        {
            print("Error while enumerating files : \(error.localizedDescription)")
        }
    }
    
    func openLocalFile(filePath: String)
    {
        self.fileView = UIView(frame: CGRect(x: 0, y: self.frameSize["statusBarHeight"]!, width: self.frameSize["width"]!, height: self.frameSize["height"]!))
        self.fileView.backgroundColor = UIColor.white
        
        let buttonWidth:CGFloat = self.frameSize["width"]!
        let buttonHeight:CGFloat = 50
        
        let closeButton = UIButton(type: UIButtonType.system) as UIButton
        closeButton.setTitle("Close", for: UIControlState.normal)
        closeButton.addTarget(self, action: #selector(ViewController.closeFileViewButtonAction(_:)), for: .touchUpInside)
        closeButton.frame = CGRect(x:0, y:self.frameSize["height"]!-buttonHeight, width:buttonWidth, height:buttonHeight)
        closeButton.backgroundColor = UIColor.blue
        closeButton.tintColor = UIColor.white
        
        self.fileView.addSubview(closeButton)
        self.view.addSubview(self.fileView)
        
        let fileUrl = URL(string:filePath)
        let ext     = fileUrl!.pathExtension
        
        switch ext
        {
            case "jpg":
                do
                {
                    let imageData = try Data(contentsOf: fileUrl!)
                    let image = UIImage(data: imageData)
                    let imageView = UIImageView(image: image!)
                    imageView.frame = CGRect(x: 0, y: 0, width: self.fileView.frame.width, height: self.fileView.frame.height-50)
                    imageView.contentMode = UIViewContentMode.scaleAspectFit
                    
                    self.fileView.addSubview(imageView)
                }
                catch
                {
                    print("Error loading image : \(error)")
                }
            default:
                let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.fileView.frame.width, height: self.fileView.frame.height-50))
                let request = URLRequest(url: fileUrl!)
                webView.load(request)
                self.fileView.addSubview(webView)
        }
    }
    
    func downloadFile(fileID:String)
    {
        let url = URL(string: "https://graph.microsoft.com/v1.0/me/drive/items/" + fileID + "/content")!
        
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(self.accessToken!)",forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let _ = data, error == nil else
            {
                print("Error: \(String(describing: error))")
                return
            }
            
            if let httpUrlResponse = response as? HTTPURLResponse
            {
                if httpUrlResponse.statusCode == 200
                {
                    let header  = httpUrlResponse.allHeaderFields
                    let fileUrl = URL(string: header["Content-Location"] as! String)
                    
                    self.downloadFromURL(fileURL:fileUrl!)
                }
                else
                {
                    print("Error: status code \(httpUrlResponse.statusCode)")
                    print("Response = \(String(describing: response))")
                }
            }
        }
        task.resume()
    }
    
    func downloadFromURL(fileURL:URL)
    {
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        
        let sessionConfig   = URLSessionConfiguration.default
        let session         = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:fileURL)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            
            if let tempLocalUrl = tempLocalUrl, error == nil
            {
                if ((response as? HTTPURLResponse)?.statusCode) != nil
                {
                    self.showAlertMessage(message:"Successfully downloaded")
                }
                
                let filename            = response?.suggestedFilename
                let destinationFileUrl  = self.getValidFilePath(folderPath:documentsUrl, filename:filename!)
                
                do
                {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                }
                catch (let writeError)
                {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    self.showAlertMessage(message:"Error creating a file \(destinationFileUrl) : \(writeError)", title:"Error")
                }
            }
            else
            {
                print("Error took place while downloading a file. Error description: \(String(describing: error))");
                self.showAlertMessage(message:"Download failed", title:"Error")
            }
        }
        task.resume()
    }

    func getDataFromUrl(targetUrl: URL)->[String:String]
    {
        let components = URLComponents(url: targetUrl, resolvingAgainstBaseURL: false)!
        
        var data = [String:String]()
        
        if let queryItems = components.queryItems
        {
            for item in queryItems
            {
                data[item.name] = item.value!
            }
        }
        
        return data
    }
    
    func jsonToDictionary(jsonString: String)->[String:AnyObject]
    {
        var dataDictionary:[String:AnyObject]!
        
        if let data = jsonString.data(using: String.Encoding.utf8)
        {
            do
            {
                dataDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            }
            catch let error as NSError
            {
                print(error)
            }
        }
        
        return dataDictionary!
    }
    
    func getValidFilePath(folderPath:URL, filename:String)->URL
    {
        let fileManager = FileManager.default
        
        var destinationFileUrl          = folderPath.appendingPathComponent(filename)
        let filenameWithoutExt:String   = (destinationFileUrl.deletingPathExtension().lastPathComponent)
        let ext                         = "." + destinationFileUrl.pathExtension
        
        var index:Int = 1

        while (fileManager.fileExists(atPath: destinationFileUrl.path))
        {
            destinationFileUrl = folderPath.appendingPathComponent(filenameWithoutExt + "(\(index))" + ext)
            index += 1
        }

        return destinationFileUrl
    }
    
    func showAlertMessage(message:String, title:String = "")
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func initTableView()
    {
        self.tableViewIntialized = true
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height + 50
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height - 50
        
        myTableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight))
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        myTableView.dataSource = self
        myTableView.delegate = self
        self.view.addSubview(myTableView)
        
        let button = UIButton(type: UIButtonType.system) as UIButton
        button.setTitle("< Back", for: UIControlState.normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(ViewController.buttonActionBack(_:)), for: .touchUpInside)
        
        let xPostion:CGFloat = 0
        let yPostion:CGFloat = UIApplication.shared.statusBarFrame.size.height
        let buttonWidth:CGFloat = 100
        let buttonHeight:CGFloat = 50
        
        button.frame = CGRect(x:xPostion, y:yPostion, width:buttonWidth, height:buttonHeight)
        
        self.view.addSubview(button)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let item:[String:String] = self.tableViewContent![indexPath.row] as! [String : String]
        
        if item["type"] == "folder"
        {
            self.getFolderContent(folderID: item["id"]!)
        }
        else if let filePath = item["localUrl"]
        {
            self.openLocalFile(filePath:filePath)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewContent!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let item:[String:String] = self.tableViewContent![indexPath.row] as! [String : String]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        
        for button in (cell.subviews.filter{$0 is UIButton})
        {
            button.removeFromSuperview()
        }

        if item["type"] == "folder"
        {
            cell.textLabel!.text = "\(item["name"] as! String)(\(item["childCount"] as! String))"
            cell.textLabel?.textColor = UIColor.blue
        }
        else
        {
            cell.textLabel!.text = "\(item["name"] as! String)"
            cell.textLabel?.textColor = UIColor.black
            
            if let _ = item["id"]
            {
                let button = UIButton(type: UIButtonType.system) as UIButton
                button.setTitle("Download", for: UIControlState.normal)
                button.addTarget(self, action: #selector(ViewController.buttonAction(_:)), for: .touchUpInside)
                button.tag = indexPath.row
                button.tintColor = UIColor.blue
                button.frame = CGRect(x:self.myTableView.frame.size.width - 100, y:0, width:100, height:50)
                
                cell.addSubview(button)
            }
        }
        return cell
    }
    
    @objc func buttonAction(_ sender:UIButton!)
    {
        let item = self.tableViewContent![sender.tag] as! [String : String]
        self.downloadFile(fileID: item["id"]!)
        sender.tintColor = UIColor.gray
    }
    
    @objc func buttonActionBack(_ sender:UIButton!)
    {
        if let folderId = self.parentFolderID
        {
            self.getFolderContent(folderID: folderId)
        }
        else
        {
            self.loadMenu()
            self.myTableView.removeFromSuperview()
            self.tableViewIntialized = false
        }
    }
    
    @objc func menuButtonAction(_ sender:UIButton!)
    {
        switch sender.tag
        {
            case 1:
                (self.accessToken == nil)
                    ? self.openAuthorizationPage()
                    : self.getFolderContent()
                break
            case 2:
                self.openLocalDirectory()
                break
            default:
                break
        }
        self.menuView.removeFromSuperview()
    }
    
    @objc func closeFileViewButtonAction(_ sender:UIButton!)
    {
        self.fileView.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

